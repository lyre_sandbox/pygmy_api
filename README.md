<p>
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

## Prerequisites

* Run docker with postgres db or install manually
* install dotenv cli (needed to load the correct .env file for prisma)
```bash
yarn global add dotenv-cli
```

## Installation
```bash
$ yarn install
```
* Create .env or .env.dev file in project root path
* Fill with content
  Example:
```
### Required fields
  POSTGRES_USER=<name>
  POSTGRES_PASSWORD=<password>
  POSTGRES_DB=<dbname>
  DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@localhost:5432/${POSTGRES_DB}"
### Optional
  NODE_ENV=development
  PORT=4005
  UPDATE_AFTER_RT_DAYS=<number>
  EXPIRED_RT_DAYS=<number>
  EXPIRED_AT_MINUTES=<number>
  JWT_AT_SECRET=SUPER_AT_SECRET
  JWT_RT_SECRET=SUPER_RT_SECRET
```
* run prisma command to initialize database tables and initial data
```bash
yarn prisma:push
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Description

For a fun project learning how to use different technologies
