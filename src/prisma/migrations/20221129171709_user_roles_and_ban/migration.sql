-- CreateEnum
CREATE TYPE "Role" AS ENUM ('user', 'admin', 'moder');

-- AlterTable
ALTER TABLE "User"
    ADD COLUMN "ban"   BOOLEAN NOT NULL DEFAULT false,
    ADD COLUMN "roles" "Role"[] DEFAULT ARRAY ['user']::"Role"[];
