/* eslint-disable */
export default (): SystemConfig => ({
  settings: {
    NODE_ENV:
      process.env.NODE_ENV === 'production' ? 'production' : 'development',
    PORT: process.env.PORT ? parseInt(process.env.PORT) : 4005,
  },
  prisma: {
    POSTGRES_USER: process.env.POSTGRES_USER!,
    POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD!,
    POSTGRES_DB: process.env.POSTGRES_DB!,
    DATABASE_URL: process.env.DATABASE_URL!,
  },
  auth: {
    UPDATE_AFTER_RT_DAYS: process.env.UPDATE_AFTER_RT_DAYS
      ? parseInt(process.env.UPDATE_AFTER_RT_DAYS)
      : 7,
    EXPIRED_RT_DAYS: process.env.EXPIRED_RT_DAYS
      ? parseInt(process.env.EXPIRED_RT_DAYS)
      : 14,
    EXPIRED_AT_MINUTES: process.env.EXPIRED_AT_MINUTES
      ? parseInt(process.env.EXPIRED_AT_MINUTES)
      : 15,
    JWT_AT_SECRET: process.env.JWT_AT_SECRET || 'at-secret',
    JWT_RT_SECRET: process.env.JWT_RT_SECRET || 'rt-secret',
  },
});

interface SystemConfig {
  settings: GlobalSettings;
  prisma: PrismaConfig;
  auth: AuthConfig;
}

export interface GlobalSettings {
  NODE_ENV: Mode;
  PORT: number;
}

export interface PrismaConfig {
  POSTGRES_USER: string;
  POSTGRES_PASSWORD: string;
  POSTGRES_DB: string;
  DATABASE_URL: string;
}

export interface AuthConfig {
  EXPIRED_RT_DAYS: number;
  EXPIRED_AT_MINUTES: number;
  UPDATE_AFTER_RT_DAYS: number;
  JWT_AT_SECRET: string;
  JWT_RT_SECRET: string;
}

export type Mode = 'production' | 'development';
