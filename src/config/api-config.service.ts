/* eslint-disable */
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthConfig, GlobalSettings, PrismaConfig } from './configuration';

@Injectable()
export class ApiConfigService {
  constructor(private configService: ConfigService) {}

  get settings(): GlobalSettings {
    return this.configService.get('settings')!;
  }

  get auth(): AuthConfig {
    return this.configService.get('auth')!;
  }

  get prisma(): PrismaConfig {
    return this.configService.get('prisma')!;
  }
}
