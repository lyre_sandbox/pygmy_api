import { plainToInstance } from 'class-transformer';
import {
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Min,
  validateSync,
} from 'class-validator';

export default function (config: Record<string, unknown>) {
  const validatedConfig = plainToInstance(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });

  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
    validationError: { target: false },
  });

  if (errors.length > 0) {
    const errStr = JSON.stringify(errors)
      .split('},')
      .join('},\n')
      .replace('[{', '[\n{')
      .replace('}]', '}\n]');
    throw new Error(
      `with ${process.env.NODE_ENV === 'production' ? '.env' : '.env.dev'},
      Problems: ${errStr}`,
    );
  }

  return validatedConfig;
}

enum Environment {
  Development = 'development',
  Production = 'production',
}

class EnvironmentVariables {
  @IsEnum(Environment)
  @IsOptional()
  NODE_ENV: Environment;

  @IsNumber()
  @Min(3000)
  @IsOptional()
  PORT: number;

  @IsOptional()
  @IsString()
  JWT_AT_SECRET: string;

  @IsOptional()
  @IsString()
  JWT_RT_SECRET: string;

  @IsString()
  POSTGRES_USER: string;

  @IsString()
  POSTGRES_PASSWORD: string;

  @IsString()
  POSTGRES_DB: string;

  @IsString()
  DATABASE_URL: string;

  @IsNumber()
  @Min(0)
  @IsOptional()
  UPDATE_AFTER_RT_DAYS: number;

  @IsNumber()
  @Min(1)
  @IsOptional()
  EXPIRED_RT_DAYS: number;

  @IsNumber()
  @Min(5)
  @IsOptional()
  EXPIRED_AT_MINUTES: number;
}
