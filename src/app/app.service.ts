import { Injectable } from '@nestjs/common';
import { ApiConfigService } from '../config/api-config.service';

@Injectable()
export class AppService {
  constructor(private config: ApiConfigService) {}
  test1() {
    const MODE = this.config.settings.NODE_ENV;
    return `hello from api (mode: ${MODE}`;
  }
}
