import { Module } from '@nestjs/common';
import { PrismaModule } from 'nestjs-prisma';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApiConfigService } from '../config/api-config.service';
import configuration from '../config/configuration';
import configValidate from '../config/config-validate';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { WallpaperModule } from '../wallpaper/wallpaper.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: process.env.NODE_ENV === 'production' ? '.env' : '.env.dev',
      load: [configuration],
      validate: configValidate,
      expandVariables: true,
      cache: true,
    }),
    PrismaModule.forRootAsync({
      isGlobal: true,
      useFactory(config: ConfigService) {
        return {
          prismaOptions: {
            datasources: {
              db: {
                url: config.get('DATABASE_URL'),
              },
            },
            log: ['info', 'warn', 'error', 'query'],
            // middlewares: [loggingMiddleware()],
          },
        };
      },
      inject: [ConfigService],
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'src/schema.gql',
      sortSchema: true,
      // playground: false,
      // plugins: [ApolloServerPluginLandingPageLocalDefault()],
      subscriptions: {
        'graphql-ws': true,
      },
      buildSchemaOptions: {
        numberScalarMode: 'integer',
      },
      // context: (): ApiContext => {
      //   return { user: { id: 5 } };
      // },
    }),
    WallpaperModule,
    UserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService, ApiConfigService],
})
export class AppModule {}
