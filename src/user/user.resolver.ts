import { Resolver } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User } from './entities/user.entity';

@Resolver(() => User)
export class UserResolver {
  constructor(private readonly _userService: UserService) {}

  // @ResolveField()
  // wallpaper(@Root() u: User) {
  //   return this.userService.findWallpaper(u.id);
  // }

  // @Mutation(() => User, { name: 'createUser' })
  // async createUser(
  //   @Args('createUserInput') data: CreateUserInput,
  // ): Promise<User> {
  //   return await this.userService.create(data);
  // }

  // @Query(() => [User], { name: 'users' })
  // async findAll(@Info() info): Promise<User[]> {
  //   const select = new PrismaSelect(info).value;
  //   console.log(select);
  //   const res = this.userService.findAll(select);
  //   console.log(await res);
  //   return res;
  // // }
  //
  // @Query(() => User, { name: 'user' })
  // findOne(@Args('id', { type: () => Int }) id: number) {
  //   return this.userService.findOne(id);
  // }
  //
  // @Mutation(() => User)
  // updateUser(@Args('updateUserInput') updateUserInput: UpdateUserInput) {
  //   return this.userService.update(updateUserInput.id);
  // }
  //
  // @Mutation(() => User)
  // removeUser(@Args('id', { type: () => Int }) id: number) {
  //   return this.userService.remove(id);
  // }
}
