import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { PrismaSelect } from '@paljs/plugins';
import { AuthBaseServiceOptions } from '../auth/auth.types';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async findByEmail(
    email: string,
    { gqlInfo, gqlSelect }: AuthBaseServiceOptions = {},
  ) {
    const select = gqlInfo
      ? new PrismaSelect(gqlInfo).value
      : gqlSelect
      ? gqlSelect
      : {};
    return this.prisma.user.findUnique({
      where: { email },
      ...select,
    });
  }

  async findById(
    userId: number,
    { gqlInfo, gqlSelect }: AuthBaseServiceOptions = {},
  ) {
    const select = gqlInfo
      ? new PrismaSelect(gqlInfo).value
      : gqlSelect
      ? gqlSelect
      : {};
    return await this.prisma.user.findUnique({
      where: { id: userId },
      ...select,
    });
  }
}
