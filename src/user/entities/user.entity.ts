import { Field, ObjectType } from '@nestjs/graphql';
import { Wallpaper } from '../../wallpaper/entities/wallpaper.entity';
import { JwtRefreshToken } from '../../auth/entities/jwt-refersh-token.entity';
import { RoleEnum, RoleType } from '../../auth/auth.types';

@ObjectType()
export class User {
  @Field()
  id: number;

  @Field()
  createdAt: Date;

  @Field()
  email: string;

  @Field({ nullable: true })
  firstname: string;

  @Field(() => String, { nullable: true })
  lastname: string | null;

  @Field(() => [Wallpaper], { nullable: 'items' })
  wallpapers: Wallpaper[];

  @Field()
  ban: boolean;

  @Field(() => [RoleEnum])
  roles: RoleType[];

  password: string;
  refresh_tokens: JwtRefreshToken[];
}
