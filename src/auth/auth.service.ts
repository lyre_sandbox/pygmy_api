import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { AuthLoginInput } from './dto/auth-login.input';
import { AuthRegisterInput } from './dto/auth-register.input';
import { AuthHelper } from './auth.helper';
import { PartialAuthPayload } from './entities/auth-payload.entity';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { BaseJwtUser } from './dto/jwt-user.dto';
import { SaveRTData } from './entities/jwt-refersh-token.entity';
import { AuthPayloadServiceOptions, LogoutInp } from './auth.types';
import { ApiConfigService } from '../config/api-config.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly config: ApiConfigService,
  ) {}

  /**
   * @param input
   * @param options #Required for working auth from multiple devices, otherwise only works with the last used device*/
  async login(
    input: AuthLoginInput,
    { gqlSelect = {}, user_agent = 'anonym' }: AuthPayloadServiceOptions = {},
  ): Promise<PartialAuthPayload> {
    const user = await this.userService.findByEmail(input.email, {
      gqlSelect: {
        select: { ...gqlSelect.select, password: true, id: true, email: true },
      },
    });
    if (!user)
      throw new NotFoundException(
        `User with email: ${input.email} does not exist`,
      );

    const passwordValid = await AuthHelper.validate(
      input.password,
      user.password,
    );
    if (!passwordValid) throw new BadRequestException('Invalid password');

    const tokens = await this.getTokens({ sub: user.id, email: user.email });

    await this.removeTokenFromDevice(user_agent, user.id);
    await this.saveToken({
      expiredAt: this.genExpiredDate(),
      user_agent,
      userId: user.id,
      refresh_token: tokens.refresh_token,
    });
    return {
      user,
      tokens,
    };
  }

  /**
   * @param input
   * @param gqlInfo
   * @param gqlSelect
   * @param user_agent #Required to working auth from multiple devices, otherwise only works with the last used device*/
  async register(
    input: AuthRegisterInput,
    { gqlSelect = {}, user_agent }: AuthPayloadServiceOptions = {},
  ): Promise<PartialAuthPayload> {
    if (input.password !== input.confirmPassword)
      throw new BadRequestException("Passwords don't match");
    delete input.confirmPassword;

    const hashedPassword = await AuthHelper.hash(input.password);

    const createdUser = await this.prisma.user.create({
      data: {
        ...input,
        password: hashedPassword,
      },
      select: { ...gqlSelect.select, id: true, email: true },
    });

    const tokens = await this.getTokens({
      sub: createdUser.id,
      email: createdUser.email,
    });

    await this.saveToken({
      expiredAt: this.genExpiredDate(),
      refresh_token: tokens.refresh_token,
      user_agent: user_agent || 'anonym',
      userId: createdUser.id,
    });

    return {
      user: createdUser,
      tokens,
    };
  }

  async refresh(
    refresh_token: string,
    { gqlSelect = {} }: AuthPayloadServiceOptions = {},
  ): Promise<PartialAuthPayload> {
    const tokenWithUser = await this.prisma.jwtToken.findUnique({
      where: { refresh_token },
      include: {
        user: { select: { ...gqlSelect.select, id: true, email: true } },
      },
    });
    if (!tokenWithUser) throw new ForbiddenException('Token is not valid');

    const tokens = await this.getTokens({
      sub: tokenWithUser.user.id,
      email: tokenWithUser.user.email,
    });

    const isNeeded = this.checkTokenNecessaryUpdate(tokenWithUser.expiredAt);
    if (isNeeded) {
      const token = {
        ...tokenWithUser,
        user: undefined,
        id: undefined,
      };
      await this.updateToken(
        {
          ...token,
          refresh_token: tokens.refresh_token,
          expiredAt: this.genExpiredDate(),
        },
        refresh_token,
      );
    }

    return {
      user: tokenWithUser.user,
      tokens: {
        access_token: tokens.access_token,
        refresh_token: isNeeded
          ? tokens.refresh_token
          : tokenWithUser.refresh_token,
      },
    };
  }

  async logout({ isFull, userId, refresh_token }: LogoutInp): Promise<boolean> {
    if (isFull) {
      await this.prisma.jwtToken.deleteMany({
        where: {
          userId,
        },
      });
      return true;
    } else {
      await this.prisma.jwtToken.delete({
        where: { refresh_token },
      });
      return true;
    }
  }

  private checkTokenNecessaryUpdate(expiredAt: Date): boolean {
    const { UPDATE_AFTER_RT_DAYS, EXPIRED_RT_DAYS } = this.config.auth;
    const today = new Date();
    const updateDay = new Date(expiredAt);

    updateDay.setDate(
      updateDay.getDate() - EXPIRED_RT_DAYS + UPDATE_AFTER_RT_DAYS,
    );
    const diff = updateDay.getTime() - today.getTime();
    return diff <= 0;
  }

  private genExpiredDate(): Date {
    const days = this.config.auth.EXPIRED_RT_DAYS;
    return new Date(new Date().getTime() + days * 86400000);
  }

  private async saveToken(data: SaveRTData) {
    return this.prisma.jwtToken.create({
      data,
    });
  }

  private async updateToken(data: SaveRTData, oldToken?: string) {
    //todo need to remove expired tokens from bd in some scheduled task
    if (oldToken) {
      const token = await this.prisma.jwtToken.findUnique({
        where: { refresh_token: oldToken },
      });
      if (!token) return this.saveToken(data);
      return this.prisma.jwtToken.update({
        where: { id: token.id },
        data,
      });
    }
    return this.saveToken(data);
  }

  private async getTokens(payload: BaseJwtUser) {
    const { EXPIRED_RT_DAYS, EXPIRED_AT_MINUTES } = this.config.auth;
    const [access_token, refresh_token] = await Promise.all([
      this.jwtService.signAsync(payload, {
        secret: process.env.JWT_AT_SECRET || 'secret',
        expiresIn: 60 * EXPIRED_AT_MINUTES,
      }),
      this.jwtService.signAsync(payload, {
        secret: process.env.JWT_RT_SECRET || 'secret',
        expiresIn: EXPIRED_RT_DAYS * 24 * 60 * 60,
      }),
    ]);
    return {
      access_token,
      refresh_token,
    };
  }

  private removeTokenFromDevice(user_agent: string, userId: number) {
    return this.prisma.jwtToken.deleteMany({ where: { user_agent, userId } });
  }
}
