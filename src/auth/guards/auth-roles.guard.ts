import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ROLES_KEY } from '../decorators/auth-permission.decorator';
import { GqlAuthGuard } from './gql-auth.guard';
import { RoleType } from '../auth.types';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { CtxUser } from '../../common/types/context.type';
import { CompareHelper } from '../../common/helpers/compare.helper';

@Injectable()
export class AuthRolesGuard extends GqlAuthGuard implements CanActivate {
  constructor(private jwtService: JwtService, private reflector: Reflector) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    let isTokenValid = false;
    let havePermission = false;
    const requiredRoles = this.reflector.getAllAndOverride<RoleType[]>(
      ROLES_KEY,
      [context.getHandler(), context.getClass()],
    );

    try {
      const check = await super.canActivate(context);
      const execCtx = GqlExecutionContext.create(context);
      const ctx = execCtx.getContext();
      const req = ctx.req;
      isTokenValid = typeof check === 'boolean' ? check : false;
      if (!isTokenValid) return false;
      if (!requiredRoles) return isTokenValid;

      const user: CtxUser = req.user;
      ctx.user = user;
      havePermission = CompareHelper.arrInclSameStr(requiredRoles, user.roles);
    } catch (err) {
      if ((err as any)?.message === 'jwt expired')
        throw new UnauthorizedException('Token expired');
      return false;
    }
    return isTokenValid && havePermission;
  }
}
