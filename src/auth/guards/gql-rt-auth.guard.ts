import { AuthGuard } from '@nestjs/passport';
import { CanActivate, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JWT_RT } from '../auth.constants';

export class GqlRtAuthGuard extends AuthGuard(JWT_RT) implements CanActivate {
  getRequest(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req;
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const res = await super.canActivate(context);
    const execCtx = GqlExecutionContext.create(context);
    const ctx = execCtx.getContext();
    ctx.user = ctx.req.user;
    return typeof res === 'boolean' ? res : false;
  }
}
