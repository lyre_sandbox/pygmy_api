import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtUser } from '../dto/jwt-user.dto';
import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JWT_AT } from '../auth.constants';
import { CtxUser } from '../../common/types/context.type';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class AccessTokenStrategy extends PassportStrategy(Strategy, JWT_AT) {
  constructor(private prisma: PrismaService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_AT_SECRET || 'secret',
    });
  }

  async validate(payload: JwtUser): Promise<CtxUser> {
    if (!payload.sub && !payload.email) throw new UnauthorizedException();
    const user = await this.prisma.user.findUnique({
      where: { id: payload.sub },
      select: {
        id: true,
        refresh_tokens: true,
        roles: true,
        email: true,
        ban: true,
      },
    });
    if (!user || user.ban) throw new UnauthorizedException();
    if (user.ban) throw new ForbiddenException('You are banned');
    return user;
  }
}
