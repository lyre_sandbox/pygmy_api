import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtUser } from '../dto/jwt-user.dto';
import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { FastifyRequest } from 'fastify';
import { JWT_RT } from '../auth.constants';
import { CtxUser } from '../../common/types/context.type';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(Strategy, JWT_RT) {
  constructor(private prisma: PrismaService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_RT_SECRET || 'secret',
      passReqToCallback: true,
    });
  }

  async validate(req: FastifyRequest, payload: JwtUser): Promise<CtxUser> {
    if (!payload.sub && !payload.email) throw new UnauthorizedException();
    const user = await this.prisma.user.findUnique({
      where: { id: payload.sub },
      select: {
        id: true,
        refresh_tokens: true,
        roles: true,
        email: true,
        ban: true,
      },
    });
    if (!user) throw new UnauthorizedException();
    if (user.ban) throw new ForbiddenException('You are banned');
    return user;
  }
}
