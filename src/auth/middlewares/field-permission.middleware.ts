import { FieldMiddleware, MiddlewareContext, NextFn } from '@nestjs/graphql';
import { RoleType } from '../auth.types';
import { CtxUser } from '../../common/types/context.type';
import {
  ForbiddenException,
  InternalServerErrorException,
} from '@nestjs/common';
import { CompareHelper } from '../../common/helpers/compare.helper';

export const fieldPermissionMiddleware: FieldMiddleware = async (
  ctx: MiddlewareContext,
  next: NextFn,
) => {
  const { info, context } = ctx;
  const { extensions } = info.parentType.getFields()[info.fieldName];
  const permissions = extensions?.permissions as RoleType[] | undefined;
  const user: CtxUser | undefined = context?.user;
  let isAllowed = false;
  if (!user)
    throw new ForbiddenException(
      `Unauthorized user does not have sufficient permissions to access "${info.fieldName}" field`,
    );
  if (!permissions)
    throw new InternalServerErrorException(
      'Permissions not granted for protected field',
    );
  isAllowed = CompareHelper.arrInclSameStr(permissions, user.roles);
  if (!isAllowed) {
    throw new ForbiddenException(
      `User does not have sufficient permissions to access "${info.fieldName}" field.`,
    );
  }
  return next();
};
