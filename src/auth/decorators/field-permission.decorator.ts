import { Extensions } from '@nestjs/graphql';
import { RoleType } from '../auth.types';

export const FieldPermission = (...roles: RoleType[]) => {
  const permissions = { permissions: roles };
  return Extensions(permissions);
};
