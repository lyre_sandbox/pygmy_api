import { createParamDecorator } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthCtx, CtxUser } from '../../common/types/context.type';

export const UserDec = createParamDecorator(
  (_data, ctx): CtxUser =>
    GqlExecutionContext.create(ctx).getContext<AuthCtx>().req.user,
);
