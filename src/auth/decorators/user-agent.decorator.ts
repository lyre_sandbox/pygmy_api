import { createParamDecorator } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Ctx } from '../../common/types/context.type';

export const UserAgent = createParamDecorator((_data, ctx) => {
  const req = GqlExecutionContext.create(ctx).getContext<Ctx>().req;
  return req.headers['user-agent'];
});
