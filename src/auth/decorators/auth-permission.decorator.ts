import { SetMetadata } from '@nestjs/common';
import { RoleType } from '../auth.types';

export const ROLES_KEY = 'roles';

export const Permission = (...roles: RoleType[]) =>
  SetMetadata(ROLES_KEY, roles);
