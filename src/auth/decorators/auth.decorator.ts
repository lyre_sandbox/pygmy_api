import { UseGuards } from '@nestjs/common';
import { AuthRolesGuard } from '../guards/auth-roles.guard';

export const Auth = () => UseGuards(AuthRolesGuard);
