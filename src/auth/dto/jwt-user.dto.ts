export class BaseJwtUser {
  sub: number;
  email: string;
}

export class JwtUser extends BaseJwtUser {
  iat: number;
  exp: number;
}
