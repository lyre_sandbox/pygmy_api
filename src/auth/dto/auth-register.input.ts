import { InputType, Field } from '@nestjs/graphql';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  MaxLength,
  MinLength,
} from 'class-validator';

@InputType()
export class AuthRegisterInput {
  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsNotEmpty()
  password: string;

  @Field({ nullable: true })
  @MinLength(3)
  @MaxLength(30)
  @IsOptional()
  firstname?: string;

  @Field({ nullable: true })
  @MinLength(3)
  @MaxLength(30)
  @IsOptional()
  lastname?: string;

  @Field()
  @IsNotEmpty()
  confirmPassword?: string;
}
