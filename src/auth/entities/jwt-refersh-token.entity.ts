import { Field, ObjectType, OmitType } from '@nestjs/graphql';

@ObjectType()
export class JwtRefreshToken {
  @Field()
  id: number;

  @Field()
  userId: number;

  /**
   * #unique field
   * */
  @Field()
  refresh_token: string;

  @Field()
  expiredAt: Date;

  @Field()
  user_agent: string;
}

export class SaveRTData extends OmitType(JwtRefreshToken, ['id']) {
  id?: never;
  user?: never;
}
