import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AccessJwtToken {
  @Field()
  accessToken: string;
}
