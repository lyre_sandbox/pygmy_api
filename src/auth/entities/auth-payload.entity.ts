import { Field, ObjectType } from '@nestjs/graphql';
import { User } from '../../user/entities/user.entity';
import { JwtTokens } from './jwt-tokens.entity';

@ObjectType()
export class AuthPayload {
  @Field(() => User)
  user: User;

  @Field(() => JwtTokens)
  tokens: JwtTokens;
}

export class PartialAuthPayload {
  user: Partial<User>;
  tokens: Partial<JwtTokens>;
}
