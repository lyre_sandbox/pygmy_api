import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { UserModule } from '../user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { GqlRtAuthGuard } from './guards/gql-rt-auth.guard';
import { GqlAuthGuard } from './guards/gql-auth.guard';
import { AccessTokenStrategy } from './strategies/access-token.strategy';
import { RefreshTokenStrategy } from './strategies/refresh-token.stategy';
import { ApiConfigService } from '../config/api-config.service';

@Module({
  imports: [JwtModule.register({}), UserModule],
  providers: [
    AuthService,
    AuthResolver,
    AccessTokenStrategy,
    RefreshTokenStrategy,
    GqlRtAuthGuard,
    GqlAuthGuard,
    ApiConfigService,
  ],
})
export class AuthModule {}
