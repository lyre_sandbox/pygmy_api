import {
  Args,
  Context,
  Info,
  Mutation,
  Query,
  Resolver,
} from '@nestjs/graphql';
import { AuthLoginInput } from './dto/auth-login.input';
import { AuthRegisterInput } from './dto/auth-register.input';
import {
  AuthPayload,
  PartialAuthPayload,
} from './entities/auth-payload.entity';
import { AuthService } from './auth.service';
import { BadRequestException, UseGuards } from '@nestjs/common';
import { GqlRtAuthGuard } from './guards/gql-rt-auth.guard';
import { AuthCtx, CtxUser } from '../common/types/context.type';
import { UserAgent } from './decorators/user-agent.decorator';
import { User } from '../user/entities/user.entity';
import { UserService } from '../user/user.service';
import { GraphQLResolveInfo } from 'graphql/type';
import { PrismaSelect } from '@paljs/plugins';
import { HeadersHelper } from '../common/helpers/headers.helper';
import { UserDec } from './decorators/ctx-user.decorator';
import { Auth } from './decorators/auth.decorator';

@Resolver()
/*todo add services support for handle gql req info */
//todo need configure service exceptions
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Auth()
  @Query(() => User)
  async me(
    @UserDec() ctxUser: CtxUser,
    @Info() info: GraphQLResolveInfo,
  ): Promise<Partial<User>> {
    const user = await this.userService.findById(ctxUser.id, {
      gqlInfo: info,
    });
    if (!user) throw new BadRequestException('User is not found');
    return user;
  }

  @Mutation(() => AuthPayload)
  login(
    @UserAgent()
    user_agent: string,
    @Args({ name: 'input', type: () => AuthLoginInput }) input: AuthLoginInput,
    @Info() info: GraphQLResolveInfo,
  ): Promise<PartialAuthPayload> {
    const { select } = new PrismaSelect(info).value;
    return this.authService.login(input, {
      user_agent,
      gqlSelect: select.user,
    });
  }

  @Mutation(() => AuthPayload)
  register(
    @UserAgent()
    user_agent: string,
    @Args({ name: 'input', type: () => AuthRegisterInput })
    input: AuthRegisterInput,
    @Info() info: GraphQLResolveInfo,
  ): Promise<PartialAuthPayload> {
    const { select } = new PrismaSelect(info).value;
    return this.authService.register(input, {
      user_agent,
      gqlSelect: select.user,
    });
  }

  @UseGuards(GqlRtAuthGuard)
  @Mutation(() => AuthPayload)
  refresh(
    @Context() ctx: AuthCtx,
    @Info() gqlInfo: GraphQLResolveInfo,
  ): Promise<PartialAuthPayload> {
    const token = HeadersHelper.parseBearerToken(ctx.req.headers.authorization);
    if (!token) throw new Error('problem with auth token');
    const { select } = new PrismaSelect(gqlInfo).value;
    return this.authService.refresh(token.value, { gqlSelect: select.user });
  }

  @UseGuards(GqlRtAuthGuard)
  @Mutation(() => Boolean)
  async logout(
    @Context() ctx: AuthCtx,
    @Args('isFull', { defaultValue: false }) isFull: boolean,
  ) {
    const token = HeadersHelper.parseBearerToken(ctx.req.headers.authorization);
    if (!token) throw new Error('problem with auth token');
    return await this.authService.logout({
      userId: ctx.user.id,
      refresh_token: token.value,
      isFull,
    });
  }
}
