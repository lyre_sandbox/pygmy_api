import { JwtRefreshToken } from './entities/jwt-refersh-token.entity';
import { ServiceOptions } from '../common/types/service-options.type';
import { Prisma } from '@prisma/client';
import { registerEnumType } from '@nestjs/graphql';

export type TokenSaveMeta = Partial<
  Omit<JwtRefreshToken, 'refresh_token' | 'userId' | 'id'>
>;

export interface LogoutInp {
  userId: number;
  /**
   * Logout from all devices*/
  isFull?: boolean;
  refresh_token: string;
}
export type AuthServiceOptions = AuthBaseServiceOptions & TokenSaveMeta;
export interface AuthBaseServiceOptions extends ServiceOptions {
  gqlSelect?: { select?: Prisma.UserSelect };
}
export type AuthPayloadServiceOptions = Omit<
  AuthBaseServiceOptions,
  'gqlInfo'
> &
  TokenSaveMeta;

export enum RoleEnum {
  admin = 'admin',
  user = 'user',
  moder = 'moder',
}

export type RoleType = 'admin' | 'user' | 'moder';

registerEnumType(RoleEnum, { name: 'RoleEnum' });
