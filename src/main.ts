import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { PrismaService } from 'nestjs-prisma';
import { ValidationPipe } from '@nestjs/common';
import { ApiConfigService } from './config/api-config.service';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );
  const { PORT } = app.get(ApiConfigService).settings;
  const prismaService: PrismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);

  app.useGlobalPipes(new ValidationPipe());
  await app.listen(PORT, () => {
    console.log(`GraphQL Playground: http://localhost:${PORT}/graphql`);
  });
}

bootstrap();
