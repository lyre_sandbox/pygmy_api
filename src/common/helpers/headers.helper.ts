export class HeadersHelper {
  static parseBearerToken(rawToken: any): BearerToken | null {
    if (typeof rawToken === 'string') {
      const matches = rawToken.match(this.tokenRegEx);
      if (matches && matches[1] === 'Bearer')
        return { scheme: matches[1], value: matches[2] };
    }
    return null;
  }

  private static tokenRegEx = /(\S+)\s+(\S+)/;
}

interface BearerToken extends Token {
  scheme: 'Bearer';
}

interface Token {
  scheme: string;
  value: string;
}
