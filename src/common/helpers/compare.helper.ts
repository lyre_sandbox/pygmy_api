export class CompareHelper {
  static arrInclSameStr(arr1: string[], arr2: string[]): boolean {
    for (const r of arr2) if (arr1.includes(r)) return true;
    return false;
  }
}
