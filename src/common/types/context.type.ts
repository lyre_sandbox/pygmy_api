import { FastifyRequest } from 'fastify';
import { User } from '../../user/entities/user.entity';

export interface Ctx {
  req: FastifyRequest;
}

export interface AuthCtx {
  req: ApiRequest;
  user: CtxUser;
}

export interface ApiRequest extends FastifyRequest {
  user: CtxUser;
}

export type CtxUser = Pick<
  User,
  'email' | 'refresh_tokens' | 'id' | 'roles' | 'ban'
>;
