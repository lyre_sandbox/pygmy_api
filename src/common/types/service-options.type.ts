import { GraphQLResolveInfo } from 'graphql/type';

export interface ServiceOptions {
  gqlInfo?: GraphQLResolveInfo;
  gqlSelect?: Record<string, any>;
}
