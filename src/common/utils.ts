export function range(start: number, end: number, options: Options = {}) {
  const { inclusive = false, step = 1 } = options;
  return {
    [Symbol.iterator]() {
      return this;
    },
    next() {
      if (start < end || (inclusive && start <= end)) {
        const curr = start;
        start = start + step;
        return { done: false, value: curr };
      }
      return { done: true, value: end };
    },
  };
}

interface Options {
  step?: number;
  inclusive?: boolean;
}
