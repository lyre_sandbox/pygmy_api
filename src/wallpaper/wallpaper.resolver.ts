import { Args, Info, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { WallpaperService } from './wallpaper.service';
import { PartialWallpaper, Wallpaper } from './entities/wallpaper.entity';
import { CreateWallpaperInput } from './dto/create-wallpaper.input';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from '../auth/guards/gql-auth.guard';
import { UserDec } from '../auth/decorators/ctx-user.decorator';
import { GraphQLResolveInfo } from 'graphql/type';
import { Permission } from '../auth/decorators/auth-permission.decorator';
import { Auth } from '../auth/decorators/auth.decorator';
import { CtxUser } from '../common/types/context.type';

@Resolver(() => Wallpaper)
export class WallpaperResolver {
  constructor(private readonly wallpaperService: WallpaperService) {}

  @UseGuards(GqlAuthGuard)
  @Mutation(() => Wallpaper)
  async createWallpaper(
    @Args('input')
    input: CreateWallpaperInput,
    @UserDec()
    user: CtxUser,
  ) {
    return this.wallpaperService.create(user.id, input);
  }

  @Query(() => [Wallpaper], { name: 'wallpaper' })
  async findAll() {
    const res = this.wallpaperService.findAll();
    console.log(await res);
    return res;
  }

  @Auth()
  @Permission('user')
  @Query(() => [Wallpaper], { name: 'mine_wallpapers' })
  async getMyWallpapers(
    @UserDec() user: CtxUser,
    @Info() info: GraphQLResolveInfo,
  ): Promise<PartialWallpaper[]> {
    return await this.wallpaperService.findManyById(user.id, {
      gqlInfo: info,
    });
  }

  @Query(() => Wallpaper, { name: 'wallpaper' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.wallpaperService.findOne(id);
  }

  // @Mutation(() => Wallpaper)
  // updateWallpaper(
  //   @Args('updateWallpaperInput') updateWallpaperInput: UpdateWallpaperInput,
  // ) {
  //   return this.wallpaperService.update(
  //     updateWallpaperInput.id,
  //     updateWallpaperInput,
  //   );
  // }

  @Mutation(() => Wallpaper)
  removeWallpaper(@Args('id', { type: () => Int }) id: number) {
    return this.wallpaperService.remove(id);
  }
}
