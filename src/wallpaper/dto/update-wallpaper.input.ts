import { CreateWallpaperInput } from './create-wallpaper.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateWallpaperInput extends PartialType(CreateWallpaperInput) {
  @Field(() => Int)
  id: number;
}
