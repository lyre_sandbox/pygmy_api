import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateWallpaperInput {
  @Field()
  name: string;

  @Field()
  image: string;
}
