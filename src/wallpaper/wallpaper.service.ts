import { Injectable } from '@nestjs/common';
import { PrismaService } from 'nestjs-prisma';
import { CreateWallpaperInput } from './dto/create-wallpaper.input';
import { ServiceOptions } from '../common/types/service-options.type';
import { PrismaSelect } from '@paljs/plugins';

@Injectable()
export class WallpaperService {
  constructor(private prisma: PrismaService) {}

  async findManyById(id: number, { gqlInfo, gqlSelect }: ServiceOptions = {}) {
    const select = gqlInfo
      ? new PrismaSelect(gqlInfo).value
      : gqlSelect
      ? gqlSelect
      : {};

    return this.prisma.wallpaper.findMany({
      where: {
        authorId: id,
      },
      ...select,
    });
  }

  create(authorId: number, data: CreateWallpaperInput) {
    return this.prisma.wallpaper.create({
      data: {
        ...data,
        authorId: authorId,
      },
    });
  }

  findAuthor(id: number) {
    return this.prisma.wallpaper.findUnique({ where: { id } }).author();
  }

  findAll() {
    return `This action returns all wallpaper`;
  }

  findOne(id: number) {
    return `This action returns a #${id} wallpaper`;
  }

  update(id: number) {
    return `This action updates a #${id} wallpaper`;
  }

  remove(id: number) {
    return `This action removes a #${id} wallpaper`;
  }
}
