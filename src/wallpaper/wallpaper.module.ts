import { Module } from '@nestjs/common';
import { WallpaperService } from './wallpaper.service';
import { WallpaperResolver } from './wallpaper.resolver';
import { AuthRolesGuard } from '../auth/guards/auth-roles.guard';
import { ApiConfigService } from '../config/api-config.service';
import { JwtService } from '@nestjs/jwt';

@Module({
  providers: [
    WallpaperResolver,
    WallpaperService,
    AuthRolesGuard,
    ApiConfigService,
    JwtService,
  ],
})
export class WallpaperModule {}
