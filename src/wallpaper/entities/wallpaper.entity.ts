import { Field, Int, ObjectType } from '@nestjs/graphql';
import { User } from '../../user/entities/user.entity';
import { FieldPermission } from '../../auth/decorators/field-permission.decorator';
import { fieldPermissionMiddleware } from '../../auth/middlewares/field-permission.middleware';

@ObjectType()
export class Wallpaper {
  @Field(() => Int)
  id: number;

  @Field()
  name: string;

  @Field()
  image: string;

  @Field(() => User, { middleware: [fieldPermissionMiddleware] })
  @FieldPermission('admin', 'moder')
  author: User;
}

export type PartialWallpaper = Partial<Wallpaper>;
